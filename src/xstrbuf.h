/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */
#ifndef _XSTRBUF_H_
#define _XSTRBUF_H_

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
// string buffer functions
//
typedef struct {
  char *data; // malloc()ed
  int used; // used bytes in data
  int size; // allocated bytes for data
} xstrbuf_t;


// <0: error; 0: ok
extern int xstrbuf_init (xstrbuf_t *b);

// <0: error; 0: ok
extern int xstrbuf_free (xstrbuf_t *b);

// <0: error; 0: ok
extern int xstrbuf_addc (xstrbuf_t *b, char c);

// <0: error; 0: ok
extern int xstrbuf_addstr (xstrbuf_t *b, const char *s);


#ifdef __cplusplus
}
#endif
#endif
