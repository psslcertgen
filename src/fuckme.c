#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <ctype.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libpolarssl/config.h"
#include "libpolarssl/entropy.h"
#include "libpolarssl/ctr_drbg.h"
#include "libpolarssl/certs.h"
#include "libpolarssl/x509.h"
#include "libpolarssl/x509write.h"
#include "libpolarssl/ssl.h"
#include "libpolarssl/net.h"
#include "libpolarssl/timing.h"
#include "libpolarssl/rsa.h"

#include "xstrbuf.h"


#define DEBUG_LEVEL 0

#define KEY_SIZE  (1024)
//#define KEY_SIZE  (128)
#define EXPONENT  (65537)


#define HTTP_RESPONSE \
  "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n\r\n" \
  "<h2>PolarSSL Test Server</h2>\r\n" \
  "<p>Successful connection using: %s</p>\r\n"


static void my_debug (void *ctx, int level, const char *str) {
  if (level < DEBUG_LEVEL) {
    fprintf((FILE *)ctx, "%s", str);
    fflush((FILE *)ctx);
  }
}


static int gen_cert (const char *hostname, x509_cert *srvcert, rsa_context *rsa) {
  x509_raw graw;
  //509_cert gcert;
  char *hbuf = malloc(strlen(hostname)+8192);
  int res;
  //
  /*
  fprintf(stderr, "SSL: generating the RSA key [%d-bit]...\n", KEY_SIZE);
  rsa_init(&rsa_key, RSA_PKCS_V15, 0);
  if ((ret = rsa_gen_key(&rsa_key, ctr_drbg_random, &ctr_drbg, KEY_SIZE, EXPONENT)) != 0) {
    fprintf(stderr, "FUCKED: rsa_gen_key returned %d\n", ret);
    goto exit;
  }
  */
  //
  /*
  sprintf(hbuf, "certs/key.%s.pem", hostname);
  res = x509write_keyfile(rsa, hbuf, X509_OUTPUT_PEM);
  if (res != 0) { fprintf(stderr, "SHIT: x509write_keyfile=%d\n", res); }
  */
  x509write_init_raw(&graw);
  //
  fprintf(stderr, "SSL: generating the certificate...\n");
  sprintf(hbuf, "CN=%s", hostname);
  //
  fprintf(stderr, "SSL: x509write_add_pubkey()\n");
  res = x509write_add_pubkey(&graw, rsa);
  if (res != 0) { fprintf(stderr, "SHIT: x509write_add_pubkey=%d\n", res); goto error; }

  fprintf(stderr, "SSL: x509write_copy_issuer()\n");
  res = x509write_copy_issuer(&graw, srvcert);
  if (res != 0) { fprintf(stderr, "SHIT: x509write_copy_issuer=%d\n", res); goto error; }

  fprintf(stderr, "SSL: x509write_add_subject()\n");
  res = x509write_add_subject(&graw, (unsigned char *)hbuf);
  if (res != 0) { fprintf(stderr, "SHIT: x509write_add_subject=%d\n", res); goto error; }

  fprintf(stderr, "SSL: x509write_add_validity()\n");
  res = x509write_add_validity(&graw, (unsigned char *)"2013-01-01 12:00:00", (unsigned char *)"2014-01-01 12:00:00");
  if (res != 0) { fprintf(stderr, "SHIT: x509write_add_validity=%d\n", res); goto error; }

  //fprintf(stderr, "SSL: x509write_create_selfsign()\n");
  //res = x509write_create_selfsign(&graw, rsa);
  //if (res != 0) { fprintf(stderr, "SHIT: x509write_create_selfsign=%d\n", res); goto error; }

  fprintf(stderr, "SSL: x509write_create_sign()\n");
  res = x509write_create_sign(&graw, rsa);
  if (res != 0) { fprintf(stderr, "SHIT: x509write_create_sign=%d\n", res); goto error; }

/*
  fprintf(stderr, "SSL: x509parse_crt_der()\n");
  res = x509parse_crt_der(&gcert, (const unsigned char *)graw.raw.data, graw.raw.len);
  if (res != 0) { fprintf(stderr, "SHIT: x509parse_crt_der=%d\n", res); goto error; }
*/

  sprintf(hbuf, "certs/cert.%s.pem", hostname);
  fprintf(stderr, "SSL: x509write_crtfile()\n");
  res = x509write_crtfile(&graw, hbuf, X509_OUTPUT_PEM);
  if (res != 0) { fprintf(stderr, "SHIT: x509write_crtfile=%d\n", res); goto error; }

  fprintf(stderr, "SSL: x509write_free_raw()\n");
  x509write_free_raw(&graw);
  //res = x509write_crtfile(&gcert, hbuf, X509_OUTPUT_PEM);
  //if (res != 0) { fprintf(stderr, "SHIT: x509write_crtfile=%d\n", res); }
  //
  //x509write_init_raw( &cert );
  //x509write_add_pubkey( &cert, rsa );
  //x509write_add_subject( &cert, "CN='localhost'" );
  //x509write_add_validity( &cert, "2007-09-06 17:00:32", "2010-09-06 17:00:32" );
  //x509write_create_selfsign( &cert, rsa );
  //x509write_crtfile( &cert, "cert.der", X509_OUTPUT_DER );
  //x509write_crtfile( &cert, "cert.pem", X509_OUTPUT_PEM );
  //x509write_free_raw( &cert );
  return 0;
error:
  free(hbuf);
  x509write_free_raw(&graw);
  return -1;
}


int main (int argc, char *argv[]) {
  int ret, len;
  int listen_fd;
  int client_fd = -1;
  unsigned char buf[1024], *wpos;
  const char *pers = "fuckme";
  //
  entropy_context entropy;
  ctr_drbg_context ctr_drbg;
  x509_cert srvcert;
  rsa_context rsa;
  ssl_context ssl;
  //
  signal(SIGCHLD, SIG_IGN);
  //
  entropy_init(&entropy);
  if ((ret = ctr_drbg_init(&ctr_drbg, entropy_func, &entropy, (const unsigned char *)pers, strlen(pers))) != 0) {
    fprintf(stderr, "FUCKED: ctr_drbg_init returned %d\n", ret);
    goto exit;
  }
  //
  rsa_init(&rsa, RSA_PKCS_V15, 0);
  ret = x509parse_keyfile(&rsa, "certs/key.pem", NULL);
  if (ret != 0) {
    fprintf(stderr, "FUCKED: x509parse_key returned %d\n", ret);
    goto exit;
  }
  //
  memset(&srvcert, 0, sizeof(srvcert));
  ret = x509parse_crtfile(&srvcert, "certs/cert.pem");
  if (ret != 0) {
    fprintf(stderr, "FUCKED: x509parse_crt returned %d\n", ret);
    goto exit;
  }
  fprintf(stderr, "cert.pem loaded\n");
  //
  if (access("certs/cert.localhost.pem", R_OK) != 0) {
    if (gen_cert("localhost", &srvcert, &rsa) != 0) {
      fprintf(stderr, "FUCKED: can't generate certificate for localhost!\n");
      goto exit;
    }
  }
  //
  ret = x509parse_crtfile(&srvcert, "certs/cert.localhost.pem");
  if (ret != 0) {
    fprintf(stderr, "FUCKED: x509parse_crt returned %d\n", ret);
    goto exit;
  }
  fprintf(stderr, "cert.localhost.pem loaded\n");
  //
  if ((ret = pssl_net_bind(&listen_fd, NULL, 4433)) != 0) {
    fprintf(stderr, "FUCKED: pssl_net_bind returned %d\n", ret);
    goto exit;
  }
  //
  for (;;) {
    pid_t pid;
    xstrbuf_t hdrs;
    //
    memset(&ssl, 0, sizeof(ssl));
    if ((ret = pssl_net_accept(listen_fd, &client_fd, NULL)) != 0) {
      fprintf(stderr, "FUCKED: pssl_net_accept returned %d\n", ret);
      goto exit;
    }
    pid = fork();
    if (pid < 0) {
      fprintf(stderr, "FUCKED: fork returned %d\n", pid);
      goto exit;
    }
    if (pid != 0) {
      // parent
      if ((ret = ctr_drbg_reseed(&ctr_drbg, (const unsigned char *)"parent", 6)) != 0) {
        fprintf(stderr, "FUCKED: ctr_drbg_reseed returned %d\n", ret);
        goto exit;
      }
      close(client_fd);
      continue;
    }
    // child
    close(listen_fd);
    //
    if ((ret = ctr_drbg_reseed(&ctr_drbg, (const unsigned char *)"child", 5)) != 0) {
      fprintf(stderr, "FUCKED: ctr_drbg_reseed returned %d\n", ret);
      goto exit;
    }
    if ((ret = ssl_init(&ssl)) != 0) {
      fprintf(stderr, "FUCKED: ssl_init returned %d\n\n", ret);
      goto exit;
    }
    ssl_set_endpoint(&ssl, SSL_IS_SERVER);
    ssl_set_authmode(&ssl, SSL_VERIFY_NONE);
    ssl_set_rng(&ssl, ctr_drbg_random, &ctr_drbg);
    ssl_set_dbg(&ssl, my_debug, stdout);
    ssl_set_bio(&ssl, pssl_net_recv, &client_fd, pssl_net_send, &client_fd);
    if (srvcert.next == NULL) {
      fprintf(stderr, "SSL: we DON'T have two certificates!\n");
      goto exit;
    }
    ssl_set_ca_chain(&ssl, &srvcert, NULL, NULL);
    ssl_set_own_cert(&ssl, srvcert.next, &rsa);
    //
    fprintf(stderr, "SSL: performing handshake...\n");
    while ((ret = ssl_handshake(&ssl)) != 0) {
      if (ret != POLARSSL_ERR_NET_WANT_READ && ret != POLARSSL_ERR_NET_WANT_WRITE) {
        fprintf(stderr, "FUCKED: ssl_handshake returned %d\n", ret);
        goto exit;
      }
    }
    //
    fprintf(stderr, "SSL: reading headers from client...\n");
    xstrbuf_init(&hdrs);
    for (;;) {
      char c;
      ret = ssl_read(&ssl, (void *)&c, 1);
      if (ret == POLARSSL_ERR_NET_WANT_READ || ret == POLARSSL_ERR_NET_WANT_WRITE) continue;
      if (ret <= 0) {
        switch (ret) {
          case POLARSSL_ERR_SSL_PEER_CLOSE_NOTIFY:
            fprintf(stderr, "SSL: connection was closed gracefully\n");
            break;
          case POLARSSL_ERR_NET_CONN_RESET:
            fprintf(stderr, "SSL: connection was reset by peer\n");
            break;
          default:
            fprintf(stderr, "SSL: ssl_read returned %d\n", ret);
            break;
        }
        break;
      }
      xstrbuf_addc(&hdrs, c);
      if (hdrs.used >= 2 && hdrs.data[hdrs.used-1] == '\n') {
        if (hdrs.data[hdrs.used-2] == '\n') break; // headers complete
        if (hdrs.data[hdrs.used-2] == '\r' && hdrs.used >= 3 && hdrs.data[hdrs.used-3] == '\n') break; // headers complete
      }
    }
    fprintf(stderr, "=== HEADERS ===\n%s===\n", hdrs.data);
    xstrbuf_free(&hdrs);
    //
    fprintf(stderr, "SSL: writing to client...\n");
    len = sprintf((char *)buf, HTTP_RESPONSE, ssl_get_ciphersuite(&ssl));
    wpos = buf;
    while (len > 0) {
       while ((ret = ssl_write(&ssl, wpos, len)) <= 0) {
         if (ret == POLARSSL_ERR_NET_CONN_RESET) {
           fprintf(stderr, "SSL: peer closed the connection\n");
           goto exit;
         }
         if (ret != POLARSSL_ERR_NET_WANT_READ && ret != POLARSSL_ERR_NET_WANT_WRITE) {
           fprintf(stderr, "SSL: ssl_write returned %d\n", ret);
           goto exit;
         }
       }
       len -= ret;
       wpos += ret;
    }
    ssl_close_notify(&ssl);
    goto exit;
  }
exit:
  pssl_net_close(client_fd);
  x509_free(&srvcert);
  rsa_free(&rsa);
  ssl_free(&ssl);
  //
  return ret;
}
